import express from 'express';
import translate from 'translate';
import bodyParser from 'body-parser';

const app = express();

const bodyTextParser = bodyParser.text();

function middleware1(req, res, next){
	console.log('this is the first middleware');
	next(req.query.skipMiddleware ? 'route' : '');
}

function middleware2(req, res, next){
	console.log('this is the second middleware');
	next();
}

function middleware3(req, res, next){
	console.log('this is the third middleware');
	next();
}

app.get('/', (req, res) => {
	res.send('Welcome to my webservice');
});

app.get('/hello', (req, res) => {
	res.send('Hello back!');
});

app.post('/translate/:lang?', middleware1, middleware2, middleware3, bodyTextParser, async (req, res) => {
	const inputText = req.body;
	const lang = req.params.lang || 'fr';
	res.send(await translate(inputText, lang));
});

app.post('/translate/:lang?', bodyTextParser, async (req, res) => {
	res.send(`SKIPPED`);
});

app.use((req, res) => {
	res.status(404);
	res.send(`An ‘extremely credible source’ has called my office and told me that Barack Obama’s placeholder text is a fraud. You could see there was text coming out of her eyes, text coming out of her wherever.`);
});

app.listen(1234, () => {
	console.log('Server is running on port 1234');
});