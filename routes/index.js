import cors from 'cors';

import api from './api/index.js';

function initRoutes(app){
	app.use(cors());
	app.get('/', (req, res, next) => {
		res.send(`Welcome to my service!`);
	});
	app.use('/api', api);
	app.use((req, res) => {
		res.status(404);
		res.send(`An ‘extremely credible source’ has called my office and told me that Barack Obama’s placeholder text is a fraud. You could see there was text coming out of her eyes, text coming out of her wherever.`);
	});
}

export default initRoutes;