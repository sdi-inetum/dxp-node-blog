import express from 'express';
import translate from "translate";
import {bodyToText} from "../middlewares/parsers.js";

const router = express.Router();

router.post('/:lang?', bodyToText, async (req, res) => {
	const inputText = req.body;
	const lang = req.params.lang || 'fr';
	res.send(await translate(inputText, lang));
});

export default router;