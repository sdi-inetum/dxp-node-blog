import express from 'express';
import cors from 'cors';

import articles from './articles.js';
import hello from './hello.js';
import translate from './translate.js';

const router = express.Router();

router.use(cors());
router.use('/articles', articles);
router.use('/hello', hello);
router.use('/translate', translate);

export default router;