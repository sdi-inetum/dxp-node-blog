import express from 'express';
import {addBlogPost, updateBlogPost, deleteBlogPost, getBlogPosts, getBlogPost} from '../../helpers/articles.js';
import {bodyToJson} from "../middlewares/parsers.js";

const router = express.Router();

router.get('/:postid?', async (req, res) => {
	const posts = req.params.postid ?
		await getBlogPost(req.params.postid) :
		await getBlogPosts(req.query);
	res.json(posts);
});

router.post('/:postid?', bodyToJson, async (req, res) => {
	const postResult = req.params.postid ?
		await updateBlogPost(req.params.postid, req.body) :
		await addBlogPost(req.body);
	res.json(postResult);
});

router.delete('/:postid', async (req, res) => {
	const deleteResult = await deleteBlogPost(req.params.postid);
	res.json(deleteResult);
});

export default router;
