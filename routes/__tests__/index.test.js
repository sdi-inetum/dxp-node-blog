import app from '../../app.js';
import {getBlogPost, getBlogPosts, addBlogPost, deleteBlogPost, updateBlogPost} from '../../helpers/articles.js';
import request from 'supertest';

describe(`ROUTES`, ()=>{

	test(`WHEN INDEX route is called`, async () => {
		const result = await request(app).get('/');
		expect(result.text).toMatchSnapshot();
	});

	describe(`WHEN BLOG routes are called`, ()=>{

		const dummyPost = {
			"name": "Hendrik De Permentier",
			"email": "hendrikdepermentier@gmail.com",
			"description": "foobar",
			"message": "Booooooh!!"
		};
		let dummmyDoc;

		beforeAll(async ()=>{
			dummmyDoc = await addBlogPost({...dummyPost});
		});

		test('Add blogpost', async () => {
			const myPost = {...dummyPost, description: 'foobar2', message: 'SOME OTHER POST'};
			const result = await request(app).post('/api/articles').send(myPost);
			const blogpost = await getBlogPost(result.body.insertedId.toString());
			expect(blogpost).toMatchObject(myPost);
			expect(typeof blogpost.updated).toBe('number');
		});

		test('Get all blogposts', async () => {
			const result = await request(app).get('/api/articles');
			const posts = result.body;
			expect(Array.isArray(posts)).toBeTruthy();
			expect(posts.at(-1)).toMatchObject(dummyPost);
		});

		test('Get one blogpost', async () => {
			const result = await request(app).get(`/api/articles/${dummmyDoc.insertedId.toString()}`);
			expect(result.body).toMatchObject(dummyPost);
			expect(Object.keys(result.body)).toMatchSnapshot();
		});

		test('Update blogpost', async () => {
			const result = await request(app).post(`/api/articles/${dummmyDoc.insertedId.toString()}`).send({...dummyPost, message: 'Yay'});
			const blogpost = await getBlogPost(dummmyDoc.insertedId.toString());
			expect(blogpost.message).toBe('Yay');
		});


		test('Delete blogpost', async () => {
			const result = await request(app).delete(`/api/articles/${dummmyDoc.insertedId.toString()}`);
			const blogpost = await getBlogPost(dummmyDoc.insertedId.toString());
			expect(blogpost).toBeFalsy();
		});

	});

});