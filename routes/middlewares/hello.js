function middleware1(req, res, next){
	console.log('this is the first middleware');
	next(req.query.skipMiddleware ? 'route' : '');
}

function middleware2(req, res, next){
	console.log('this is the second middleware');
	next();
}

function middleware3(req, res, next){
	console.log('this is the third middleware');
	next();
}

export {middleware1, middleware2, middleware3};