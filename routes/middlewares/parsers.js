import express from "express";
const bodyToText = express.text();
const bodyToJson = express.json();
export {bodyToJson, bodyToText};