function sum(num1, num2){
	return num1 + num2;
}

function sumAll(arrNumbers){
	return arrNumbers.reduce((acc, num) => {
		return acc + num;
	}, 0);
}

export default {sum, sumAll};