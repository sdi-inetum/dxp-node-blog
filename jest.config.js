export default {
	name: 'Our first nodejs server',
	verbose: true,
	collectCoverage: true,
	collectCoverageFrom: [
		'**/*.js'
	],
	preset: "@shelf/jest-mongodb"
};