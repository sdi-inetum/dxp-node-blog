import math from '../math.js';

describe(`GIVEN A mathematics util`, ()=>{

	describe( `WHEN function sum is called`, () => {

		beforeAll(() => {
			console.log('I run before all tests');
		});

		beforeEach(() => {
			console.log('I run before each tests');
		});

		test(`THEN parameter a should be added to b`, ()=>{
			expect(math.sum(5,6)).toBe(11);
		});

		test(`THEN negative numbers should be taken into account`, ()=>{
			expect(math.sum(5,-6)).toBe(-1);
		});

	});

	describe(`WHEN function sumAll is called`, ()=>{

		test(`THEN all numbers in array should be summed`, ()=>{
			const result = math.sumAll([5,4,12,5,48,65,1,3]);
			expect(result).toBe(105);
		})

	});

});

describe(`Another test to check the before all/each behaviour`, ()=>{

	beforeAll(() => {
		console.log('Before all 2');
	});

	beforeEach(() => {
		console.log('Before each 2');
	});

	test('Test something', ()=>{
		expect(6+5).toBe(11);
	});

});