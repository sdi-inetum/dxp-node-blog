import express from 'express';
import translate from 'translate';
import bodyParser from 'body-parser';

const app = express();

app.use(bodyParser.text());

app.use((req, res, next) => {
	console.log('Hello from the other side');
	req.foo = "bar";
	next();
});

app.use((req, res, next) => {
	console.log(`next middleware, value of req.foo is "${req.foo}"`);
	next();
});

app.get('/', (req, res) => {
	res.send('Welcome to my webservice');
});

app.get('/hello', (req, res) => {
	res.send('Hello back!');
});

app.use((req, res, next) => {
	console.log('Middleware after the hello route');
	next();
});

app.post('/translate/:lang?', async (req, res) => {
	const inputText = req.body;
	const lang = req.params.lang || 'fr';
	res.send(await translate(inputText, lang));
});

app.use((req, res) => {
	res.status(404);
	res.send(`An ‘extremely credible source’ has called my office and told me that Barack Obama’s placeholder text is a fraud. You could see there was text coming out of her eyes, text coming out of her wherever.`);
});

app.listen(1234, () => {
	console.log('Server is running on port 1234');
});