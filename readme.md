#DXP GFiAcademyNode Blog Server
<strong>Original author:</strong> Hendrik De Permentier

<strong>Adapted to low.js</strong>: Seppe Dijkmans
***

To install all dependencies
`npm install`

To start the server 
`npm start`

To test the server
http://localhost:1234/api/hello

***
##Troubleshooting

Recommended Node Version `v14.5.4`