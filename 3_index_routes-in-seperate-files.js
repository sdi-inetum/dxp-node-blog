import express from 'express';
import routes from './routes/index.js';

const app = express();
routes(app);

app.listen(1234, () => {
	console.log('Server is running on port 1234');
});