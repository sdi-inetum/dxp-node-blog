import http from 'http';
import translate from 'translate';

const server = http.createServer((req, res) => {
	let requestBody = '';
	req.on('data', chunk => {
		console.log('receiving chunk', chunk);
		requestBody += chunk;
	});
	req.on('end', ()=>{
		console.log('received all of it!');
		console.log(requestBody);
		console.log('send my response');
		res.writeHead(200, {'content-type': 'text/plain'});
		res.write(`hello from route ${req.url}`);
		res.write('\n\n');
		const lang = req.url.replace('/', '');
		res.write(`Translate to: ${lang}`);
		res.write('\n\n');
		translate(requestBody, lang).then(translated => {
			res.write(translated);
			res.end();
		});
	});
});

server.listen(1234);
