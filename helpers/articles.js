import {getDocument, getDocuments, updateDocument, addDocument, deleteDocument} from './lowdb.js';

const COLLECTION_NAME = 'articles';

function addBlogPost(content){
	const created = new Date().getTime();
	content.updated = content.created = created;
	return addDocument(COLLECTION_NAME, content);
}

function updateBlogPost(postId, content){
	const updated = new Date().getTime();
	content.updated = updated;
	return updateDocument(COLLECTION_NAME, postId, content);
}

function deleteBlogPost(postId){
	return deleteDocument(COLLECTION_NAME, postId);
}

function getBlogPosts(params){
	return getDocuments(COLLECTION_NAME, params.from, params.n);
}

function getBlogPost(postId){
	return getDocument(COLLECTION_NAME, postId);
}

export {addBlogPost, updateBlogPost, deleteBlogPost, getBlogPosts, getBlogPost};