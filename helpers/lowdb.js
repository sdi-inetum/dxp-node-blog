import { join, dirname } from 'path'
import { Low, JSONFile } from 'lowdb'
import { fileURLToPath } from 'url'

const __dirname = dirname(fileURLToPath(import.meta.url));

// Use JSON file for storage
const file = join(__dirname, '../database/db.json');
const adapter = new JSONFile(file);
const db = new Low(adapter);


const addDocument = async (key, content) => {
    await db.read();
    const id = db.data && db.data[key] ? db.data[key].length : 0;
    db.data[key].push({ id, ...content });
    await db.write()
};

const getDocuments = async (key) => {
    await db.read();
    db.data = db.data || {
        articles: [{
            "id": 0,
            "title": "Sample blog",
            "content": "This is a sample blog",
            "created": 1645620969468,
            "updated": 1645620969468
        }]
    };
    return db.data[key];
};

const getDocument = async (key, id) => {
    await db.read();
    return db.data[key].find(document => document.id == id);
};

const updateDocument = async (key, value) => {
    return true;
};

const deleteDocument = async (key, id) => {
    await db.read();
    const index = db.data[key].findIndex((document) => document.id == id);
    db.data[key].splice(index, 1);
    await db.write();
    return true;
};

export { addDocument, getDocument, getDocuments, updateDocument, deleteDocument };